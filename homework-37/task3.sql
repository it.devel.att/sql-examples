SELECT
YEAR(`actions`.`action_date`) 'Год',
`categories`.`category`,
ROUND(SUM(`actions`.`qty` * `actions`.`price`)/SUM(`actions`.`qty`), 2) 'Среднее за ед. товара',
MAX(`actions`.`price`) 'Макс. цена за ед.',
MIN(`actions`.`price`) 'Мин. цена за ед.'
FROM
`actions`
LEFT JOIN `products` ON `products`.`id` = `actions`.`product_id`
LEFT JOIN `categories` ON `categories`.`id` = `products`.`category_id`
GROUP BY YEAR(`actions`.`action_date`), `categories`.`category`
ORDER BY `categories`.`category`, YEAR(`actions`.`action_date`)
;