SELECT
`brands`.`brand`,
sum(`actions`.`price` * `actions`.`qty`) 'Сумма поставки'
FROM `actions`
LEFT JOIN `products` ON  `products`.`id`= `actions`.`product_id`
LEFT JOIN `brands` ON `products`.`brand_id` = `brands`.`id`
GROUP BY `brands`.`brand`
;