SELECT
`suppliers`.`supplier` 'Поставщик',
`categories`.`category` 'Категория товара',
SUM(`actions`.`qty` * `actions`.`price`) 'Товара на сумму'
FROM `actions`
LEFT JOIN `suppliers` ON `suppliers`.`id` = `actions`.`supplier_id`
LEFT JOIN `products` ON `products`.`id` = `actions`.`product_id`
LEFT JOIN `categories` ON `categories`.`id` =  `products`.`category_id`
WHERE YEAR(`actions`.`action_date`) = 2016
GROUP BY `suppliers`.`supplier`, `categories`.`category`;
;