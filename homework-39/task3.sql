SELECT
`categories`.`category`
FROM `categories`
WHERE `categories`.`id` IN 
(
 SELECT
 `products`.`category_id`
 FROM `products`
 WHERE `products`.`id` IN 
 (
  SELECT
  DISTINCT
  `actions`.`product_id`
  FROM `actions`
  WHERE 
  YEAR(`actions`.`action_date`) = 2016 AND
  MONTH(`actions`.`action_date`) > 5 AND MONTH(`actions`.`action_date`) < 9 AND
  `actions`.`supplier_id` = (SELECT `id` FROM `suppliers` WHERE `supplier` LIKE 'ID%')
 )
)
;
