SELECT `suppliers`.`supplier`
FROM `suppliers`
WHERE `id` IN
( 
 SELECT 
 `actions`.`supplier_id`
 FROM `actions`
 WHERE YEAR(`actions`.`action_date`) = 2016
 GROUP BY `actions`.`supplier_id`
 HAVING SUM(`actions`.`price` * `actions`.`qty`) >
 (
  SELECT
  SUM(`actions`.`price` * `actions`.`qty`)
  FROM `actions`
  WHERE YEAR(`actions`.`action_date`) = 2016
  AND `actions`.`supplier_id` = (SELECT `id` FROM `suppliers` WHERE `supplier` LIKE 'ID%')
 )
)
;