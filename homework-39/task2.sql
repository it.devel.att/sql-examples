SELECT `products`.`product`
FROM `products`
WHERE `products`.`id` IN 
(
 SELECT 
 DISTINCT
 `actions`.`product_id`
 FROM `actions`
 WHERE `actions`.`product_id` NOT IN 
	(
	SELECT `actions`.`product_id`
    FROM `actions`
    WHERE `actions`.`supplier_id` = (SELECT `id` FROM `suppliers` WHERE `supplier` LIKE 'ID%')
    )
)
;