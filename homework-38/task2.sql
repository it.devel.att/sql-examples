SELECT 
IF(`year` = '', `supplier`, '') 'Поставщик',
IF(`category` = '', `year`, '') 'Год',
`category` 'Категория товара',
`sum` 'Сумма'
FROM(
SELECT 
`suppliers`.`supplier` AS `supplier`,
'' AS `year`,
'' AS `category`,
ROUND(SUM(`actions`.`price` * `actions`.`qty`), 2) AS `sum`
FROM
`actions`
INNER JOIN `suppliers` ON `suppliers`.`id` = `actions`.`supplier_id`
GROUP BY `supplier`

UNION
SELECT
`suppliers`.`supplier` AS `supplier`,
YEAR(`actions`.`action_date`) AS `year`,
'' AS `category`,
ROUND(SUM(`actions`.`price` * `actions`.`qty`), 2) AS `sum`
FROM
`actions`
INNER JOIN `suppliers` ON `suppliers`.`id` = `actions`.`supplier_id`
GROUP BY `supplier`, `year`

UNION
SELECT
`suppliers`.`supplier` AS `supplier`,
YEAR(`actions`.`action_date`) AS `year`,
`categories`.`category` AS `category`,
ROUND(SUM(`actions`.`price` * `actions`.`qty`), 2)
FROM
`actions`
INNER JOIN `suppliers` ON `suppliers`.`id` = `actions`.`supplier_id`
INNER JOIN `products` ON `products`.`id` = `actions`.`product_id`
INNER JOIN `categories` ON `categories`.`id` = `products`.`category_id` 
GROUP BY `supplier`, `year`, `category`
ORDER BY `supplier`, `year`, `category`
) AS `task2`
;