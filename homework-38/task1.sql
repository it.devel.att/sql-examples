SELECT
IF(`Month` = '', `Year`, '') 'Год',
IF(`Day` = '', `Month`, '') 'Месяц',
`Day` 'День',
`Sum` 'Сумма'
FROM (
SELECT
YEAR(`year`.`action_date`) AS `Year`,
'' AS `Month`,
'' AS `Day`,
ROUND(SUM(`year`.`price` * `year`.`qty`), 2) AS `Sum`
FROM `actions` AS `year`
GROUP BY YEAR(`year`.`action_date`)

UNION
SELECT
YEAR(`month`.`action_date`),
CASE MONTH(`month`.`action_date`) WHEN 1 THEN 'январь' WHEN 2 THEN 'февраль' WHEN 3 THEN 'март'
WHEN 4 THEN 'апрель' WHEN 5 THEN 'май' WHEN 6 THEN 'июнь' WHEN 7 THEN 'июль' WHEN 8 THEN 'август' WHEN 9 THEN 'сентябрь'
WHEN 10 THEN 'октябрь' WHEN 11 THEN 'ноябрь' WHEN 12 THEN 'декабрь' 
END AS `Month`,
'' AS `day`,
ROUND(SUM(`month`.`price` * `month`.`qty`), 2)
FROM `actions` AS `month`
GROUP BY YEAR(`month`.`action_date`), `Month`

UNION
SELECT
YEAR(`day`.`action_date`),
CASE MONTH(`day`.`action_date`) WHEN 1 THEN 'январь' WHEN 2 THEN 'февраль' WHEN 3 THEN 'март'
WHEN 4 THEN 'апрель' WHEN 5 THEN 'май' WHEN 6 THEN 'июнь' WHEN 7 THEN 'июль' WHEN 8 THEN 'август' WHEN 9 THEN 'сентябрь'
WHEN 10 THEN 'октябрь' WHEN 11 THEN 'ноябрь' WHEN 12 THEN 'декабрь' 
END AS `Month`,
DATE_FORMAT(DATE(`day`.`action_date`), '%d.%m.%Y'),
ROUND(SUM(`day`.`price` * `day`.`qty`), 2)
FROM `actions` AS `day`
GROUP BY 
YEAR(`day`.`action_date`), `Month`, DATE_FORMAT(DATE(`day`.`action_date`), '%d.%m.%Y')
ORDER BY `Year`, 
CASE `Month` WHEN 'январь' THEN 1 WHEN 'февраль' THEN 2 WHEN 'март' THEN 3 WHEN 'апрель' THEN 4 WHEN 'май' THEN 5 WHEN 'июнь' THEN 6
WHEN 'июль' THEN 7 WHEN 'август' THEN 8 WHEN 'сентябрь' THEN 9 WHEN 'октябрь' THEN 10 WHEN 'ноябрь' THEN 11 WHEN 'декабрь' THEN 12
END, 
`Day`
) AS `task1`
;